```
          .         .
         ,8.       ,8.   `8.`8888.      ,8' d888888o.   8 8888          8 8888
        ,888.     ,888.   `8.`8888.    ,8'.`8888:' `88. 8 8888          8 8888
       .`8888.   .`8888.   `8.`8888.  ,8' 8.`8888.   Y8 8 8888          8 8888
      ,8.`8888. ,8.`8888.   `8.`8888.,8'  `8.`8888.     8 8888          8 8888
     ,8'8.`8888,8^8.`8888.   `8.`88888'    `8.`8888.    8 8888          8 8888
    ,8' `8.`8888' `8.`8888.   `8. 8888      `8.`8888.   8 8888          8 8888
   ,8'   `8.`88'   `8.`8888.   `8 8888       `8.`8888.  8 8888          8 8888
  ,8'     `8.`'     `8.`8888.   8 8888   8b   `8.`8888. 8 8888          8 8888
 ,8'       `8        `8.`8888.  8 8888   `8b.  ;8.`8888 8 8888          8 8888
,8'         `         `8.`8888. 8 8888    `Y8888P ,88P' 8 888888888888  8 8888
```

[![Build Status](https://travis-ci.org/VxMxPx/mysli.svg)](https://travis-ci.org/VxMxPx/mysli)

## Introduction

Mysli is a highly modular platform written in PHP5.

This is a work in progress, though at the moment is running my weblog at http://gaj.st/

## Structure

Each fragment of this system is a package, and hence easily upgradeable and replaceable.

There's much use of `static` methods, for a reason of autoloading. Final aim is
to be functional --- to have as many functions with no side-effects as possible.

## Current State

There's more to this system than it would seem at first:

- there's a very powerful template engine ([doc](https://gitlab.com/VxMxPx/mysli/blob/master/bin/mysli.tplp/doc/template-syntax.md)) and
- internationalization component ([doc](https://gitlab.com/VxMxPx/mysli/tree/master/bin/mysli.i18n/doc)).
- There's a costume, very easy to use and fast testing engine ([doc](https://gitlab.com/VxMxPx/mysli/tree/master/bin/mysli.test/doc)).
- There's a package manager which can enable/disable packages taking dependencies into account. ([example of a pkg meta](https://gitlab.com/VxMxPx/mysli/blob/master/bin/mysli.toolkit/mysli.pkg.ym))
- There's assets manager (which can be used for building and including various (fronted) assets). ([doc](https://gitlab.com/VxMxPx/mysli/blob/master/bin/mysli.assets/doc/map-file-structure.md))
- A very powerful command line support. ([doc](https://gitlab.com/VxMxPx/mysli/tree/master/bin/mysli.toolkit/doc/cli))
- Costume markdown parser (full markdown extended support). ([here](https://gitlab.com/VxMxPx/mysli/tree/master/bin/mysli.markdown/lib))
- Powerful static content generators and
- dashboard with a costume UI ([here](https://gitlab.com/VxMxPx/mysli/tree/master/bin/mysli.jui)) in making.

## Future

This project was never meant only as a CMS, it's usage is broader.
Because of structure, i.e. everying being a package, whole web-frontend can
be detached and only command line secation can be used. Another scenario is
to use only dashboard, without publically accessible frontend, for things like:
note taking, cloud storage/managment, etc...

Dashboard Prototype:<br/>
![Dashboard Prototype](http://gaj.st/assets/etc/mysli-image.png)
