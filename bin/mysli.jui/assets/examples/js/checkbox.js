mjui.add('checkbox', function() {

  'use strict';

  var ui = mysli.js.ui;
  var panel = new ui.Panel({uid: 'mjui-checkbox'});
  var titlebar = new ui.Titlebar({style: 'default'});

  // Titlebar
  titlebar.push(new ui.Button({
    icon: 'close'
  })).connect('click', function () {
    panel.close();
  });
  titlebar.push(new ui.Label({text: "Checkbox", type: ui.LabelType.Title}), {expanded: true});

  // Container
  var container = new ui.Box();
  container.push(new ui.Label({text: "Default Style"}), {padding: [true, false, false, false]});
  container.push(new ui.Checkbox({label: "Default..."}));
  container.push(new ui.Checkbox({label: "I'm disabled...", disabled: true}));
  container.push(new ui.Checkbox({label: "I'm disabled checked...", checked: true, disabled: true}));
  container.push(new ui.Checkbox({label: "I'm flat...", flat: true}));

  container.push(new ui.Label({text: "Alt Style"}), {padding: [true, false, false, false]});
  container.push(new ui.Checkbox({style: 'alt', label: "Default..."}));
  container.push(new ui.Checkbox({style: 'alt', label: "I'm disabled...", disabled: true}));
  container.push(new ui.Checkbox({style: 'alt', label: "I'm disabled checked...", checked: true, disabled: true}));
  container.push(new ui.Checkbox({style: 'alt', label: "I'm flat...", flat: true}));

  // Source
  panel.front.push(titlebar);
  panel.front.push(container, {expanded: true, padding: true});

  return panel;
});
