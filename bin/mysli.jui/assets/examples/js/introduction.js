mjui.add('introduction', function() {

  'use strict';

  var ui = mysli.js.ui,
    panel = new ui.Panel({
      uid: 'mjui-introduction',
      width: ui.PanelSize.Big,
      min_size: ui.PanelSize.Normal
    }),
    titlebar = new ui.Titlebar(),
    content = new ui.HTML();

  titlebar.push(new ui.Button({
    icon: 'close'
  })).connect('click', function () {
    panel.close();
  });
  titlebar.push(new ui.Label({text: "Introduction", type: ui.LabelType.Title}), {expanded: true});

  panel.front.push(titlebar);
  panel.front.push(content, {scroll: ui.CellScroll.Y, expanded: true});

  //content.set_busy(true);
  $.get('?html=introduction').done(function (data) {
    // content.set_busy(false);
    content.push(data);
  });

  return panel;

});
