mjui.add('navigation', function () {

  'use strict';

  var ui = mysli.js.ui;
  var panel = new ui.Panel({
    uid: 'mjui-navigation',
    width: ui.PanelSize.Small
  });
  var usermeta = new ui.Titlebar({flat: true});
  var navigation = new ui.Navigation({
    introduction: "Introduction",
    button: "Button",
    entry: "Entry",
    checkbox: "Checkbox",
    radio: "Radio",
    tabbar: "Tabbar",
    panel: "Panel"
  }, {style: 'alt'});

  panel.front.style = 'alt';

  navigation.connect('action', function (id, e, self) {
    e.stopPropagation();
    mjui.open(id);
  });

  usermeta.push(new ui.Label({text: 'Mysli JS Ui :: Developer', type: ui.LabelType.Title}));
  panel.front.push(usermeta);
  panel.front.push(navigation, {expanded: true});

  return panel;
});
