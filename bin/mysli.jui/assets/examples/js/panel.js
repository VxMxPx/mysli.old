mjui.add('panel', function () {

  'use strict';

  var ui = mysli.js.ui;
  var panel = new ui.Panel({
    uid: 'mjui-panel',
    flippable: true
  });

  // Front side of the panel
  var titlebar_front = new ui.Titlebar();
  titlebar_front.push(new ui.Button({
    icon: 'close'
  })).connect('click', function () {
    panel.close();
  });
  titlebar_front.push(new ui.Label({text: "Panel Example", type: ui.LabelType.Title}), {expanded: true});
  titlebar_front.push(new ui.Button({
    icon: 'cog'
  })).connect('click', function () {
    panel.side = ui.PanelSelectedSide.Back;
  });

  panel.front.push(titlebar_front);
  panel.front.push(new ui.HTML("Hi! I'm a panel. Use `cog` button to flip me arround!"), {padding: true, expanded: true});

  // Backside of the panel!
  var titlebar_back = new ui.Titlebar({flat: true});
  titlebar_back.push(new ui.Button({
    icon: 'arrow-left'
  })).connect('click', function () {
    panel.side = 'front';
  });
  titlebar_back.push(new ui.Label({text: "Panel Example Back", type: ui.LabelType.Title}), {expanded: true});

  panel.back.style = 'alt';
  panel.back.push(titlebar_back);
  panel.back.push(new ui.HTML("Hi! This is a backside of a panel!"), {padding: true, expanded: true});
  panel.back.push(new ui.Label({text: "Footer"}), {padding: true});

  return panel;
});
