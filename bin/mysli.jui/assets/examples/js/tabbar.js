mjui.add('tabbar', function() {

  'use strict';

  var ui = mysli.js.ui;
  var panel = new ui.Panel({uid: 'mjui-tabbar', width: ui.PanelSize.Big});
  var titlebar = new ui.Titlebar({style: 'default'});

  // Titlebar
  titlebar.push(new ui.Button({
    icon: 'close'
  })).connect('click', function () {
    panel.close();
  });
  titlebar.push(new ui.Label({text: "Tabbar Examples", type: ui.LabelType.Title}), {expanded: true});

  // Source
  panel.front.push(titlebar);
  panel.front.push(new ui.Tabbar({
    posted: {icon: "check", label: "Posted"},
    star:   {icon: 'star'},
    trash:  {icon: 'trash'}
  }, {active: 'posted'}), {expanded: true});

  return panel;
});
