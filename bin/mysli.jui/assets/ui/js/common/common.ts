/// <reference path="../../../external/jquery.d.ts" />

export function mix(defaults: any, options: any = {}): any {
  return $.extend({}, defaults, options);
}
