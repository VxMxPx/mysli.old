/// <reference path="../../../external/jquery.d.ts" />
import { Widget } from './widget';
import { Arr } from '../common/arr';
import { Prop } from '../common/prop';

export enum BoxOrientation { Horizontal, Vertical };
export enum CellScroll { Y, X, Both, None };

export class Box extends Widget {

  // Collection of contained elements
  protected collection: Arr = new Arr();

  // Element's wrapper
  protected element_wrapper: string = '<div class="ui-container-cell"></div>';

  constructor(options: any = {}) {

    super(options);

    this.element.addClass('ui-box');

    this.prop.def({
       orientation: BoxOrientation.Vertical
    });
    this.prop.push(options);

    if (this.prop.orientation === BoxOrientation.Vertical) {
      this.element.addClass('ui-box-orientation-vertical');
    }
    else {
      this.element.addClass('ui-box-orientation-horizontal');
    }
  }

  /**
   * Push widget to the container.
   */
  push(widgets: Widget | Widget[], options: any = null): Widget | Widget[] {
    return this.insert(widgets, -1, options);
  }

  /**
   * Insert widget to the container at position.
   */
  insert(widgets: Widget|Widget[], at: number, options?: any): Widget|Widget[] {

    let at_index: number;
    let class_id: string;
    let wrapped: JQuery;
    let widget: Widget;
    let cell: Cell = null;

    if (!(widgets instanceof Widget)) {
      if (widgets.constructor === Array) {
        for (let i = 0; i < (<Widget[]>widgets).length; i++) {
          this.insert(widgets[i], at, options);
        }
        return widgets;
      }
      else {
        throw new Error('Instance of widget|widgets[] is required!');
      }
    }
    else {
      widget = widgets;
    }

    // UID only, no options
    if (!options) {
      options = { uid: widget.uid }
    }
    else if (typeof options === 'string') {
      options = { uid: options };
    }
    else if (typeof options === 'object') {
      if (typeof options.uid === 'undefined') options.uid = widget.uid;
    }
    else {
      throw new Error('Invalid options provided. Null, string or {} allowed.');
    }

    if (this.collection.has(options.uid)) {
      throw new Error(`Element with such ID already exists: ${options.uid}`);
    }

    // Create classes
    class_id = 'coll-euid-' + widget.uid + ' coll-uid-' + options.uid;

    // Create wrapper, append at the end of the list
    wrapped = $(this.element_wrapper);
    wrapped.addClass(class_id);
    wrapped.append(widget.element);

    cell = new Cell(this, wrapped, options);

    // Either push after another element or at the end of the list
    at_index = (at > -1)
      ? this.collection.push_after(at, options.uid, [widget, cell])
      : this.collection.push(options.uid, [widget, cell]);

    // Either insert after particular element or just at the end
    if (at > -1) {
      this.element
        .find('.coll-euid-' + this.collection.get_from(at_index, -1).uid)
        .after(wrapped);
    }
    else {
      this.element.append(wrapped);
    }

    return widget;
  }

  /**
  * Get elements from the collection. If `cell` is provided, get cell itself.
  * @param uid  either string (uid) or number (index)
  * IDs can be chained to get to the last, by using: id1 > id2 > id3
  * All elements in chain must be of type Container for this to work.
  */
  get(uid: string|number, cell: boolean = false): Cell|Widget {

    // Used in chain
    let index_at: number;

    // Deal with a chained uid
    // Get uid of first segment in a chain, example: uid > uid2 > uid3
    if (typeof uid === 'string' && (index_at = uid.indexOf('>')) > -1) {
      let uidq: string = uid.substr(0, index_at).trim();
      let ccontainer: any = this.collection.get(uidq)[0];

      if (ccontainer instanceof Box) {
        return ccontainer.get(uid.substr(index_at + 1).trim(), cell);
      }
      else {
        throw new Error(`Failed to acquire an element. Container needed: ${uidq}`);
      }
    }

    return this.collection.get(uid)[(cell ? 1 : 0)];
  }

  /**
   * Get an element, and then remove it from the collection and DOM.
   */
  pull(uid: string|number): Widget {
    let element: Widget = <Widget>this.get(uid, false);
    this.remove(uid);
    return element;
  }

  /**
   * Check if uid is in the collection.
   */
  has(uid: string | number): boolean {
    return this.collection.has(uid);
  }

  /**
   * Remove particular cell (and the containing element)
   */
  remove(uid: string | number) {
    uid = this.collection.get(uid).uid;
    this.collection.remove(uid);
    this.element.find('.coll-euid-' + uid).remove();
  }

  get orientation(): BoxOrientation {
    return this.prop.orientation;
  }
}


export class Cell {

  protected parent: Box;
  protected $cell: JQuery;
  protected prop: any;

  constructor(parent: Box, $cell: JQuery, options: any = {}) {

    this.parent = parent;
    this.$cell = $cell;

    this.prop = new Prop({
      visible: true,
      padding: false,
      expanded: false,
      scroll: CellScroll.None
    }, this);

    this.prop.push(options, ['visible', 'padding', 'scroll', 'expanded']);
  }

  /**
   * Animate the cell.
   */
  animate(what: any, duration: number = 500, callback: any = false): void {
    this.$cell.animate(what, duration, callback);
  }

  // Get/set padded
  get padding(): boolean | any[] {
    return this.prop.padding;
  }
  set padding(value: boolean | any[]) {
    let positions: string[] = ['top', 'right', 'bottom', 'left'];

    this.$cell.css('padding', '');

    if (typeof value === 'boolean') {
      value = [value, value, value, value];
    }

    for (let i = 0; i < positions.length; i++) {
      if (typeof value[i] === 'number') {
        this.$cell.css(`padding-${positions[i]}`, value[i]);
      } else {
        this.$cell[value[i] ? 'addClass' : 'removeClass'](`pad${positions[i]}`);
      }
    }
  }

  // Get/set visibility
  get visible(): boolean {
    return this.prop.visible;
  }
  set visible(status: boolean) {

    if (status === this.prop.visible) { return; }

    this.prop.visible = status;
    this.$cell[status ? 'show' : 'hide']();
  }

  // Get/set scroll
  get scroll(): CellScroll {
    return this.prop.scroll;
  }
  set scroll(value: CellScroll) {
    switch (value) {
      case CellScroll.X:
        this.$cell.addClass('scroll-x');
        this.$cell.removeClass('scroll-y');
        this.prop.scroll = value;
        break;

      case CellScroll.Y:
        this.$cell.addClass('scroll-y');
        this.$cell.removeClass('scroll-x');
        this.prop.scroll = value;
        break;

      case CellScroll.None:
        this.$cell.removeClass('scroll-x');
        this.$cell.removeClass('scroll-y');
        this.prop.scroll = value;
        break;

      case CellScroll.Both:
        this.$cell.addClass('scroll-x');
        this.$cell.addClass('scroll-y');
        this.prop.scroll = value;
        break;

      default:
        throw new Error(`
          Invalid value required:
          Cell.(SCROLL_X|SCROLL_Y|SCROLL_BOTH|SCROLL_NONE)`);
    }
  }

  // Get/set expanded
  get expanded(): boolean {
    return this.prop.expanded;
  }
  set expanded(value: boolean) {
    this.$cell[value ? 'addClass' : 'removeClass']('expanded');
  }

  /**
   * Remove cell from a collection.
   */
  remove(): void {
    this.$cell.remove();
  }
}
