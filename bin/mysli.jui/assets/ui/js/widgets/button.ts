/// <reference path="../../../external/jquery.d.ts" />
import { Widget } from './widget';
import { IconPosition, Icon } from './icon';

export class Button extends Widget {

  protected static template: string = '<button class="ui-widget ui-button"></button>';
  protected static allowed_styles: string[] = ['primary', 'confirm', 'attention', 'default', 'alt'];

  protected $icon: JQuery;

  constructor(options: any = {}) {

    super(options);

    this.prop.def({
      // Buttons label (if any)
      label: null,
      // Weather button can be toggled
      toggle: false,
      // Weather button is pressed right now
      pressed: false,
      // Button's icon
      icon: null
    });

    // Icon can be passed in, for reasons of convinience, as an:
    // string (icon name), object (icon's options) or an Icon
    if (typeof options.icon === 'string')
      options.icon = new Icon({name: <string>options.icon});
    else if (typeof options.icon === 'object' && !(options.icon instanceof Icon))
      options.icon = new Icon(options.icon);

    this.prop.push(options, ['icon!', 'label!', 'toggle', 'pressed']);
  }

  // Get/set toggle state
  get toggle() : boolean
  {
    return this.prop.toggle;
  }
  set toggle(value: boolean)
  {
    this.prop.toggle = value;

    if (value)
      this.connect('click*self-toggle', () => {
        this.pressed = !this.pressed;
      });
    else this.disconnect('click*self-toggle');
  }

  // Get/set pressed state
  get pressed(): boolean
  {
    return this.prop.pressed;
  }
  set pressed(value: boolean)
  {
    this.prop.pressed = value;
    this.element[value ? 'addClass' : 'removeClass']('pressed');
  }

  // Get/set label
  get label(): string
  {
    return this.prop.label;
  }
  set label(value: string)
  {
    let $label: JQuery = this.element.find('span.label');
    let method: string;

    this.prop.label = value;

    if (!value)
    {
      $label.remove();
      return;
    }

    if (!$label.length)
    {
      $label = $('<span class="label" />');
      method = this.prop.icon && this.prop.icon.position === 'right' ? 'prepend' : 'append';
      this.element[method]($label);
    }

    $label.text(value);
  }

  // Get/set icon
  get icon(): Icon
  {
    return this.prop.icon;
  }
  set icon(icon: Icon)
  {
    if (!icon) return;

    if (this.prop.icon instanceof Icon) this.prop.icon.element.remove();

    let method = icon.position === IconPosition.Right ? 'append' : 'prepend';

    this.element[method](icon.element);
    this.prop.icon = icon;
  }
}
