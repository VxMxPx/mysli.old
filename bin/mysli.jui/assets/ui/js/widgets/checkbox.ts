import { GenericInput } from './generic_input';

export class Checkbox extends GenericInput {

  protected static template: string = `
    <label>
      <div class="ui-gi-input">
        <i class="fa fa-check"></i>
      </div>
      <span></span>
    </label>
  `;

  protected $checked: JQuery;

  constructor(options: any = {}) {

    super(options);

    this.$checked = this.$input.find('i');
    this.element.addClass('ui-checkbox');
    this.prop.def({
      checked: false
    });
    this.prop.push(options, ['checked']);

    this.connect('click', () => {
      if (!this.disabled) {
        this.checked = !this.checked;
      }
    });
  }

  // Get/set checked state
  get checked(): boolean
  {
    return this.prop.checked;
  }
  set checked(value: boolean)
  {
    this.prop.checked = value;
    this.$input[value ? 'addClass' : 'removeClass']('checked');
  }
}
