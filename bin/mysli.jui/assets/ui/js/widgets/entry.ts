import { GenericInput } from './generic_input';

export enum EntryType { Text, Password };

export class Entry extends GenericInput {

  protected static template: string = `
    <label>
      <span></span>
      <input class="ui-gi-input" />
    </label>
  `;

  constructor (options: any = {}) {

    super(options);

    this.element.addClass('ui-entry');

    this.prop.def({
      type: EntryType.Text,
      placeholder: null
    });

    this.prop.push(options, ['type!', 'placeholder']);
  }

  // Get/set types
  get type(): EntryType {
    return this.prop.type;
  }
  set type(value: EntryType) {
    switch (value) {
      case EntryType.Text:
        this.$input.prop('type', 'text');
        break;

      case EntryType.Password:
        this.$input.prop('type', 'password');
        break;

      default:
        throw new Error('Invalid entry type!');
    }

    this.prop.type = value;
  }

  // Get/set placeholder
  get placeholder(): string {
    return this.prop.placeholder;
  }
  set placeholder(value: string) {
    this.prop.placeholder = value;
    this.$input.prop('placeholder', value);
  }
}
