/// <reference path="../../../external/jquery.d.ts" />
import { Widget } from './widget';

export class HTML extends Widget {

  constructor(text: any = {}, options: any = {}) {
    if (text !== null && typeof text === 'object') {
      options = text;
    }
    super(options);
    this.element.addClass('ui-html');

    this.prop.def({ expanded: false });
    this.prop.push(options, ['expanded']);

    if (typeof text === 'string') {
      this.push(text);
    }
  }

  // Get/set expanded
  get expanded(): boolean {
    return this.prop.expanded;
  }
  set expanded(value: boolean) {
    this.element[value ? 'addClass' : 'removeClass']('expanded');
  }

  /**
   * Push new HTML to the container.
   * @param html
   */
  push(html: string): JQuery {

    let element: JQuery;

    // Wrap HTML in a div
    html = `<div class="ui-html-element">${html}</div>`;

    element = $(html);

    this.element.append(element);
    return element;
  }

  /**
   * Remove element(s) by specific jQuery selector.
   * @param selector
   */
  remove(selector: string): void {
    this.element.filter(selector).remove();
  }
}
