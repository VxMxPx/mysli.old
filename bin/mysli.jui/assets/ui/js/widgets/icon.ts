import { Widget } from './widget';

export enum IconPosition { Top, Right, Bottom, Left };

export class Icon extends Widget {

  protected static template: string = '<i class="fa"></i>';

  constructor(options: any = {}) {

    super(options);

    this.prop.def({
      // Icon's name
      name: null,
      // Icon's position: left, right, top, bottom
      position: IconPosition.Left,
      // Weather icont is spinning
      spin: false
    });
    this.prop.push(options, ['name!', 'position', 'spin!']);
  }

  // Get/set icon's name
  get name(): string {
    return this.prop.name;
  }
  set name(name: string) {
    this.element.removeClass("fa-"+this.prop.name);
    this.element.addClass("fa-"+name);
    this.prop.name = name;
  }

  // Get/set position
  get position(): IconPosition {
    return this.prop.position;
  }
  set position(position: IconPosition) {
    this.prop.position = position;
  }

  // Get set icon's spin
  get spin(): boolean {
    return this.prop.spin;
  }
  set spin(spin: boolean) {
    this.element.removeClass("fa-spin");
    if (spin) this.element.addClass("fa-spin");
     this.prop.spin = spin;
  }
}
