/// <reference path="../../../external/jquery.d.ts" />
import { Widget } from './widget';

export enum LabelType { Default, Title, Input };

export class Label extends Widget {
  protected static template: string = '<span class="ui-widget ui-title" />';

  constructor(options: any = {}) {
    super(options);
    this.prop.def({
      // Label's text
      text: '',
      // Type, see constants defined above
      type: LabelType.Default,
      // Weather this label is connected to an input.
      // Setting this to Widget, will force label's type to INPUT
      input: null
    });

    this.prop.push(options, ['type!', 'text!', 'input']);
  }

  // Get/set type.
  get type(): LabelType {
    return this.prop.type;
  }
  set type(type: LabelType) {
    let element: JQuery;

    switch (type) {
      case LabelType.Default:
        this.input = null;
        element = $('<span />');
        break;
      case LabelType.Title:
        this.input = null;
        element = $('<h1 />');
        break;
      case LabelType.Input:
        element = $('<label />');
        break;
      default:
        throw new Error(`Invalid type provided: ${type}`);
    }

    this.element.empty();
    this.prop.type = type;
    element.text(this.text);
    this.element.append(element);
  }

  // Get/set text.
  get text(): string {
    return this.prop.text;
  }
  set text(value: string) {
    this.prop.text = value;
    this.element.find(':first-child').text(value);
  }

  // Get/set input.
  get input(): Widget {
    return this.prop.input;
  }
  set input(widget: Widget) {
    if (!widget) {
      if (this.input) {
        this.element.find('label').prop('for', false);
        this.prop.input = null;
        this.prop.input.destroy();
      }
    }
    else {
      this.prop.input = widget;

      if (!widget.element.prop('id')) {
        widget.element.prop('id', widget.uid);
      }

      this.type = LabelType.Input;
      this.element.find('label').prop('for', widget.uid);
    }
  }
}
