import { Box, BoxOrientation } from './box';

export class PanelSide extends Box {
  constructor(options: any = {}) {
    options.orientation = BoxOrientation.Vertical;
    super(options);
    this.element.addClass('ui-panel-side');
  }
}
