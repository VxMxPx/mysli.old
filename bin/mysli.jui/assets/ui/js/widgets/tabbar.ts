import { mix } from '../common/common';
import { Widget } from './widget';
import { Box, BoxOrientation } from './box';
import { Button } from './button';

export class Tabbar extends Widget {

  protected container: Box;

  constructor(items: any, options: any = {}) {
    super(options);

    this.prop.def({
      // Which tab is active at the moment
      active: null
    });
    this.prop.push(options);

    options.orientation = BoxOrientation.Horizontal;
    this.container = new Box(options);
    this.$element = this.container.element;
    this.element.addClass('ui-tabbar');

    this.events = mix({
      // Respond to a tabbar action (tab click)
      // => ( id: string, event: any, widget: Tabbar)
      action: {}
    });

    for (let item in items) {
      if (items.hasOwnProperty(item)) {
        this.container.push(this.produce(items[item], item), item);
      }
    }
  }

  // Get/set active tab
  get active(): string {
    return this.prop.active;
  }
  set active(value: string) {
    if (this.container.has(value)) {
      if (this.prop.active) {
        (<Button>this.container.get(this.prop.active)).pressed = false;
      }
      this.prop.active = value;
      (<Button>this.container.get(value)).pressed = true;
    }
  }

  private produce(settings: any, id: string): Widget {

    if (typeof settings === 'string') settings = { label: settings };

    // set default values
    settings.uid = id;
    settings.toggle = true;
    settings.flat = true;
    settings.style = this.style;

    let button: Button = new Button(settings);

    if (this.prop.active === id) {
      button.pressed = true;
    }
    button.connect('click', (e) => {
      this.active = button.uid;
      this.trigger('action', [id, e]);
    });
    return button;
  }
}
