import { Box } from './box';

export class Titlebar extends Box {
  constructor(options: any = {}) {
    options.orientation = BoxOrientation.Horizontal;
    super(options);
    this.element.addClass('ui-titlebar');
  }

  insert(widgets: Widget|Widget[], at: number, options?: any): Widget|Widget[] {
    if (widgets.constructor === Array) {
      for (let i = 0; i < (<Widget[]> widgets).length; i++) {
        widgets[i].flat = true;
        super.insert(widgets[i], at, options);
      }
      return widgets;
    }
    else {
      (<Widget> widgets).flat = true;
      return super.insert(widgets, at, options);
    }
  }
}
