<?php

namespace mysli\jui; class __setup
{
  const __use = <<<fin
    mysli.assets.{ assets }
    mysli.toolkit.{ route }
fin;

  static function enable()
  {
    return assets::publish('mysli.jui');
  }

  static function disable()
  {
    return assets::unpublish('mysli.jui')
      and route::remove('mysli.jui::*')
      and route::write();
  }
}
