<?php

namespace mysli\jui; class jui
{
  const __use = <<<fin
    mysli.toolkit.{ request, response, output }
    mysli.toolkit.fs.{ fs, file, dir }
    mysli.tplp.{ tplp }
fin;

  /**
   * Render template or return html/js source, depending on request.
   */
  static function route()
  {
    $template = tplp::select('mysli.jui');

    if ($file = request::get('html'))   $load = [$file, 'html'];
    elseif ($file = request::get('js')) $load = [$file, 'js'];
    else                                $load = null;

    if ($load)
    {
      $source = static::read($load[0], $load[1]);

      if (!$source)
      {
        response::set_status(404);
        output::set("File not found: `{$source[0]}`");
      }
      else
      {
        response::set_status(200);
        output::set($source);
      }
    }
    else
    {
      response::set_status(200);
      $script = request::get('script', 'index');
      output::set(
        $template->render(
          'index',
          [
            'script' => static::read($script, 'js'),
            'page'   => $script
          ]
        )
      );
    }

    return true;
  }

  /**
   * Get file source from file system.
   * --
   * @param string $script
   * @param string $type html|js
   * --
   * @return string
   */
  private static function read($filename, $type)
  {
    if (!preg_match('/^[a-z0-9-_]+$/', $filename)) return null;
    $file = fs::pkgreal('mysli.jui', "/assets/examples/{$type}/{$filename}.{$type}");
    return file::exists($file) ? file::read($file) : null;
  }
}
