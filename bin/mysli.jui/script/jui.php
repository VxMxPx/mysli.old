<?php

namespace mysli\jui\root\script; class jui
{
  const __use = <<<fin
    mysli.toolkit.{ route }
    mysli.toolkit.cli.{ prog, output -> out }
fin;

  static function __run(array $args)
  {
    $prog = new prog('Mysli Jui', __CLASS__);

    $prog->set_help(true);
    $prog->set_version('mysli.jui', true);

    $prog
    ->create_parameter('--enable/-e', [
      'type'   => 'boolean',
      'def'    => false,
      'help'   => 'This will enable access to the sandbox at address: /jui.'
    ])
    ->create_parameter('--disable/-d', [
      'type'    => 'boolean',
      'def'     => false,
      'help'    => 'This will disable access to the sandbox at address: /jui.',
      'exclude' => $prog->get_parameter('--enable')
    ]);

    if (null !== ($r = prog::validate_and_print($prog, $args))) return $r;
    list($enable, $disable) = $prog->get_values('-e', '-d');

    if ($enable)
    {
      route::add('mysli.jui::route', 'ANY', '/jui/<route?:path>', 'medium')
      and route::write()
        ? out::success('Enabled', 'Visit /jui to access sandbox!')
        : out::error('Failed', 'Failed to add /jui route!');
    }
    elseif ($disable)
    {
      route::remove('mysli.jui::*')
      and route::write()
        ? out::success('Disabled', 'Web address /jui is now inaccessible!')
        : out::error('Failed', 'Cannot remove /jui route!');
    }
    else
    {
      out::warning("Please use --help to see list of available commands.");
    }

    return true;
  }
}
