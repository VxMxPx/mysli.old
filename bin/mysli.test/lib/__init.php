<?php

namespace mysli\test; class __init
{
  const __use = <<<fin
    mysli.toolkit.fs.{ fs, dir }
fin;

  static function __init()
  {
    $dir = fs::tmppath('mysli.test');
    return dir::exists($dir) or dir::create($dir);
  }
}
