<?php

#: Before
use \mysli\toolkit\cli\output;

# ------------------------------------------------------------------------------
#: Test Strong Line
#: Expect Output <<<CLI
output::strong('Hello!');
<<<CLI
[1mHello![0m

CLI;
