Tplp Template Syntax
====================

Expressions
-----------

Text in curly brackets will be evaluated:

```plaintext
// Simple variable output:
Hello, {variable}! // => Hello, <?php echo $variable; ?>
// Hello, world!!

// An array:
Your name is {user['uname']}. // => Your name is <?php echo $user['uname']; ?>
// Your name is Leonardo da Vinci.

// Multidimensional array:
{foo['bar']['baz']} // <?php echo $foo['bar']['baz']; ?>

// Variable as an array key:
{foo[bar][baz]} // <?php echo $foo[$bar][$baz]; ?>

// Object:
Your pet name is {pet->name}. // => Your pet name is <?php echo $pet->name; ?>
// Your pet name is Riki.
```

In order not to echo expression, semicolon can be used:

```plaintext
{; number} // <?php $number; ?>
```

Other expressions:

```plaintext
{''}   // => <?php echo ''; ?>
{12}   // => <?php echo 12; ?>
{-12}  // => <?php echo -12; ?>
```

Arithmetics within expression:

```plaintext
{number++}  // => <?php echo $number++; ?>
{number--}  // => <?php echo $number--; ?>
{number+12} // => <?php echo $number+12; ?>
{number-2}  // => <?php echo $number-2; ?>
{number*45} // => <?php echo $number*45; ?>
{number/34} // => <?php echo $number/34; ?>

{ product->price + product->tax - user['discount'] }
// => <?php echo $product->price + $product->tax - $user['discount']; ?>
```

String concatenation:

```plaintext
{name.' '.middlename.' '.lastname}
// => <?php echo $name.' '.$middlename.' '.$lastname; ?>
```

Variable assignation:

```plaintext
{; sum = price + tax - discount + currency }
// => <?php $sum = $price + $tax - $discount - $currency; ?>
{; greeting = 'Hi there!' } // => <?php $greeting = 'Hi there!'; ?>
{; number = -12 } // => <?php $number = -12; ?>
```

Conditions in expressions:

```plaintext
{name if name else 'Anonymous'}
// => <?php echo $name ? $name : 'Anonymous'; ?>

{user['posts']|join:' ' if user['posts']|count}
// => <?php if (count($user['posts'])) { echo implode(' ', $user['posts']); } ?>
```

Loops in expressions:

```plaintext
{post in posts}
// => <?php foreach ($posts as $post): echo $post; endforeach; ?>
{header in page['headers']}
// => <?php foreach ($page['headers'] as $header): echo $header; endforeach; ?>
{post|upper in posts}
// => <?php foreach ($posts as $post): echo strtoupper($post); endforeach; ?>
```

Filters
-------

Filters can be used in expressions. A pipe (|) symbol is used to pass a variable
to a filter (it will be passed as a first argument):

```plaintext
{name|lower} // => <?php echo strtolower($name); ?>
// LEONARDO DA VINCI => leonardo da vinci
```

Multiple filters can be chained together:

```plaintext
{name|lower|ucfirst} // => <?php echo ucfirst(strtolower($name)); ?>
// LEONARDO DA VINCI => Leonardo Da Vinci
```

Additional arguments can be passed to filters:

```plaintext
{name|slice:0,8} // => <?php echo substr($name, 0, 8); ?>
// Leonardo Da Vinci => Leonardo
```

Arguments can be variables:

```plaintext
{description|word_wrap:blog[line_width]} // => <?php echo wordwrap($description, $blog['line_width']); ?>
```

Filter can be used with non variables also:

```plaintext
{24,im_a_filter} => <?php echo im_a_filter(24); ?>
{true,im_a_filter} => <?php echo im_a_filter(true); ?>
{null,im_a_filter} => <?php echo im_a_filter(null); ?>
{'Hello World!',im_a_filter} => <?php echo im_a_filter('Hello World!'); ?>
```

Build-In Filters
----------------

### abs

Returns the absolute value of number.

```plaintext
{-12|abs} // => 12
```

### ucfirst

Make a string's first character uppercase.

```plaintext
{'hello world!'|ucfirst} // => Hello world!
```

### ucwords

Uppercase the first character of each word in a string.

```plaintext
{'hello world!'|ucwords} // => Hello World!
```

### lower

Make a string lowercase.

```plaintext
{'Hello World!'|lower} // => hello world!
```

### upper

Make a string uppercase.

```plaintext
{'Hello World!'|upper} // => HELLO WORLD!
```

### date

Format a local time/date.

{[value](http://php.net/manual/en/function.strtotime.php)|date:[format](http://php.net/manual/en/function.date.php)}

```plaintext
{'now'|date:'d.m.y'} // => 02.04.14
{'tomorrow'|date:'d.m.y'} // => 03.04.14
// $user['created_on'] = '20130201154612';
{user[created_on]|date:'d.m.y'} // => 01.02.14
```

### join

Join array elements with a string.

```plaintext
// $list = ['banana', 'orange', 'kiwi'];
{list|join:','} // => banana,orange,kiwi
```

### isset

Determine if a variable is set and is not NULL.

```plaintext
{missing_variable|isset} // False
```

### split

Split a string by string.

```plaintext
// $list = 'banana,orange,kiwi';
{list|split:','} // => ['banana', 'orange', 'kiwi']
{list|split:',',2} // => ['banana', 'orange,kiwi']
```

### length

Get string length.

```plaintext
{'hello'|length} // => 5
```

### word_count

Counts the number of words inside string.

```plaintext
{'hello world!'|word_count} // => 2
```

### count

Count all elements in an array.

```plaintext
// $animals = ['cat', 'chicken', 'dog', 'cow'];
{animals|count} // => 4
```

### nl2br

Inserts HTML line breaks before all newlines in a string.

```plaintext
// $string = "Hello\nWorld!";
{string|nl2br} // => Hello<br/>World!
```

### number_format

Format a number with grouped thousands.

```plaintext
{12000|number_format} // => 12,000
{12000|number_format:2} // => 12,000.00
{12000|number_format:4, '.', ','} // => 12.000,0000
```

See [PHP number_format](http://php.net/manual/en/function.number-format.php)
for more examples.

### replace

Returns a string produced according to the formatting string format.

{[format](http://php.net/manual/en/function.sprintf.php)|replace:variables}

```plaintext
{'The %s contains %d monkeys'|replace:'tree',12} // => The tree contains 12 monkeys.
```

### round

Rounds a float.

```plaintext
{3.4|round}         // => 3
{3.5|round}         // => 4
{3.6|round}         // => 4
{3.6|round:0}       // => 4
{1.95583|round:2}   // => 1.96
{1241757|round:-3} // => 1242000
{5.045|round:2}     // => 5.05
{5.055|round:2}     // => 5.06
```

### floor

Round fractions down.

```plaintext
{4.3|floor}   // => 4
{9.999|floor} // => 9
{-3.14|floor} // => -4
```

### ceil

Round fractions up.

```plaintext
{4.3|ceil}    // => 5
{9.999|ceil}  // => 10
{-3.14|ceil}  // => -3
```

### strip_tags

Strip HTML and PHP tags from a string.

```plaintext
{'<p>Hello world!</p>'|strip_tags} // => Hello world!
```

### show_tags

Convert special characters to HTML entities.

```plaintext
{'<p>Hello world!</p>'|show_tags} // => &lt;p&gt;Hello world!&lt;/p&gt;
```

### trim

Strip whitespace (or other characters) from the beginning and end of a string.

```plaintext
{'    Hello world!      '|trim} // => Hello world!
```

### slice

Extract a slice of the array or return part of a string.

```plaintext
// $list = ['banana', 'orange', 'kiwi', 'apple', 'strawberry']
{list|slice:0,2} // => ['banana', 'orange']
{'hello world!'|slice:0,5} // => hello
```

### word_wrap

Wraps a string to a given number of characters.

```plaintext
{'The quick brown fox jumped over the lazy dog.'|word_wrap:20} // => The quick brown fox<br />jumped over the lazy<br />dog.
```

### max

Find highest value.

```plaintext
// $list = [1, 4, 54, 3, 450, 2];
{list|max} // => 450
{10|max:80,30,2} // => 80
```

### min

Find lowest value.

```plaintext
// $list = [1, 4, 54, 3, 450, 2];
{list|min} // => 1
{10|min:80,30,2} // => 2
```

### column

Return the values from a single column in the input array.

```plaintext
// $records = [
//   ['id' => 2135, 'first_name' => 'John', 'last_name' => 'Doe'   ],
//   ['id' => 3245, 'first_name' => 'Sally', 'last_name' => 'Smith'],
//   ['id' => 5342, 'first_name' => 'Jane', 'last_name' => 'Jones' ],
//   ['id' => 5623, 'first_name' => 'Peter', 'last_name' => 'Doe'  ]
// ];

{records|column:'first_name'} // => [0 => 'John', 1 => 'Sally', 2 => 'Jane', 3 => 'Peter']
{records|column:'first_name','id'} // => [2135 => 'John', 3245 => 'Sally', 5342 => 'Jane', 5623 => 'Peter']
```

### reverse

Return an array with elements in reverse order or reverse a string.

```plaintext
// $list = [1, 2, 3, 4];
{list|reverse} // => [4, 3, 2, 1]
{'hello'|reverse} // => olleh
```

### contains

Checks if a value exists in an array or in string.

```plaintext
// $list = ['hello', 'world'];
{list|contains:'world'} // => true
{'hello world'|contains:'world'} // => true
{'hello'|contains:'world'} // => false
```

### key_exists

Checks if the given key or index exists in the array.

```plaintext
// $list = ['id' => 12, 'name' => 'Marko'];
{list|key_exists:'id'} // => true
{list|key_exists:'na'} // => false
```

### sum

Calculate the sum of values in an array.

```plaintext
// $list = [10, 20, 5];
{list|sum} // => 35
```

### unique

Removes duplicate values from an array.

```plaintext
// $list = ['one', 'two', 'one', 'three', 'one'];
{list|unique} // => ['one', 'two', 'three']
```

### range

Create an array containing a range of elements.

```plaintext
{|range:0,6} // => [0, 1, 2, 3, 4, 5, 6]
```

### random

Generate a random integer.

```plaintext
{|random:0,5} // => 0|1|2|3|4|5
```

### type

Get type of input.

```plaintext
{'foo'|type} // => string
```

Control Structures
------------------

Double colon (::) is used to start or end statement.

### If-elif-else

```php
::if variable > 0
  Above zero.
::elif variable < 0
  Bellow zero.
::else
  Zero.
::/if

<?php if ($variable > 0): ?>
  Above zero.
<?php elseif ($variable < 0): ?>
  Bellow zero.
<?php else: ?>
  Zero.
<?php endif; ?>
```

```php
::if variable > 0 and not (variable > 10)
  More than zero, but less than ten.
::/if

<?php if ($variable > 0 and !($variable > 10)): ?>
  More than zero, but less than ten.
<?php endif; ?>
```

### Inline if-else

Basic if-else example:

```php
{user[username] if user[username]|isset else 'Anonymous'}

<?php echo (isset($user['username'])) ? $user['username'] : 'Anonymous'; ?>
```

... else is optional:

```php
{user[username] if user[username]|isset}

<?php echo (isset($user['username'])) ? $user['username'] : ''; ?>
```

... complex expressions:

```php
{user[username] if user|isset and user[username]|isset else 'Anonymous'}

<?php echo (isset($user['username']) and isset($user['username'])) ? $user['username'] : 'Anonymous'; ?>
```

... translations as an output:

```php
{@ANONYMOUS if !name and not user[name]}

<?php echo (!$name and !$user['name']) ? $tplp_translator_service('ANONYMOUS') : ''; ?>
```

... complex translations:

```php
{@ANONYMOUS(count) variable[1], variable[2] if !name and not user[name]}

<?php echo (!$name and !$user['name']) ? $tplp_translator_service(['ANONYMOUS', $count], [$variable['1'], $variable['2']]) : ''; ?>
```

### For

```php
::for user in users
  Username: {user['uname']}<br/>
::/for

<?php foreach ($users as $user): ?>
  Username: <?php echo $user['uname']; ?><br/>
<?php endforeach; ?>
```

```php
::for uid,user in users
  User: {uid} => {user['uname']}<br/>
::/for

<?php foreach ($users as $udi => $user): ?>
  Username: <?php echo $user['uname']; ?><br/>
<?php endforeach; ?>
```

... if you need information about position, you can `set` a variable, which will
hold:

```plaintext
position['count']   // Number of all items
position['current'] // Current position
position['last']    // Is it last element
position['first']   // Is it first element
position['odd']     // Is current position odd
position['even']    // Is current position even
```

```plaintext
::for user in users set position
  {user['name']}{', ' if not position['last']}
::/for
```

Please note that `position` is just an example, variable can be named however
you want (as long as it is a valid PHP variable name).

### Functions

In if-elif and for statements you can use functions:

```php
::if users|count > 100
  More than 100 users!
::/if

<?php if (count($users) > 100): ?>
  More than 100 users!
<?php endif; ?>
```

```php
::for source in sources|split:'||'
  {source}
::/for

<?php foreach (explode('||', $sources) as $source): ?>
  <?php echo $source; ?>
<?php endforeach; ?>
```

Translations
------------

To use translations you need to set translator (with `set_translator`), then in
the template, translations are used the same as variables,
only prefixed with at (@) symbol.

```plaintext
{@HELLO}
```

### Pluralization

```plaintext
{@COMMENTS(12)}

// With variable:
{@COMMENTS(comments_count)}

// With variable and function(s):
{@COMMENTS(comments|count)}
```

### Variables

```plaintext
{@HELLO_USER username}
{@HELLO_USER user[uname]}
{@HELLO_USER user1, user2, false, 'string!'}
```

Comments
--------

Use curly brackets with asterisk for comments:

```plaintext
{* I'm a comment! *}
```

Escaping Characters and Regions
-------------------------------

Use backslash to escape curly brackets or apostrophe: `\{, \}, \'`

For parser to ignore particular region of your template,
use three curly brackets:

```javascript
{{{
  // Nothing in here will be parsed...
  function hello (who) {
    return 'Hello ' + who;
  }
}}}
```

Let
---

New variables can be defined and modified in template by using `let`.

```php
::let variable = 'Foo-Bar'

<?php
  // Result
  $variable = 'Foo-Bar';
?>
```

... for multi-line text:

```php
::let multiline from
  This is a multiline text,
  it will act like a text in an HTML tag.
::/let

<?php
  // Result
  $multiline = 'This is a multiline text, it will act like a text in an HTML tag.';
?>
```

... costume glue:

```php
::let items set implode(, ) from
  item one
  item two
  item three
::/let

<?php
  // Result
  $items = 'item one, item two, item three';
?>
```

... produce an array:

```php
::let an_array set array from
  one
  two
  three
::/let

<?php
  // Result
  $an_array = [ 'one', 'two', 'three' ];
?>
```

... dictionary:

```php
::let dict set dictionary(:) from
  Dublin : Ireland
  Moscow : Russia
  Ljubljana : Slovenia
  Paris : France
  Kyiv : Ukraine
::/let

<?php
  // Result
  $dict = [
    'Dublin' => 'Ireland',
    'Moscow' => 'Russia',
    'Ljubljana' => 'Slovenia',
    'Paris' => 'France',
    'Kyiv' => 'Ukraine'
  ];
?>
```

Use
---

Some package will offer utility to be used in templates,
which can be included and used with:

```plaintext
::use vendor.package
{var|package.method:parameter}
```

... or if you want to alias is:

```plaintext
::use vendor.package -> pkg
{var|pkg.method:param}
```

The class which will be included in such manner needs to have foolowing class:

```plaintext
// vendor.package/lib/__tplp.php
namespace vendor\package; class __tplp {}
```

Extend
------

file.tplp

```html
::extend layout set content
<div>
    Content
</div>
```

layout.tpl.html

```html
<html>
<body>
    ::print content
</body>
</html>
```

... result:

```html
<html>
<body>
    <div>
        Content
    </div>
</body>
</html>
```

You can extend multiple templates:

file.tpl.html

```html
::extend layout set content
::extend master set content
<div>
    Content
</div>
```

layout.tpl.html

```html
<body>
    ::print content
</body>
```

master.tpl.html

```html
<html>
::print content
</html>
```

... result:

```html
<html>
<body>
    <div>
        Content
    </div>
</body>
</html>
```

You can set additional parameters:

file.tpl.html

```html
::extend layout set content do
    ::set links
        <link rel="stylesheet" type="text/css" href="main.css">
    ::/set
::/extend
<div>
    Content
</div>
```

layout.tpl.html

```html
<html>
<head>
    ::print links
</head>
<body>
    ::print content
</body>
</html>
```

... result:

```html
<html>
<head>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div>
        Content
    </div>
</body>
</html>
```

Import
------

file.tpl.html

```html
<div class="sidebar">
    ::import sidebar
</div>
```

sidebar.tpl.html

```html
<p>I'm sidebar!</p>
```

... result:

```html
<div class="sidebar">
    <p>I'm sidebar!</p>
</div>
```

You can set additional parameters:

file.tpl.html

```html
<div class="sidebar">
    ::import sidebar do
        ::set hello
            <p>Hello world!</p>
        ::/set
    ::/import
</div>
```

sidebar.tpl.html

```html
<p>I'm sidebar!</p>
::print hello
```

... result:

```html
<div class="sidebar">
    <p>I'm sidebar!</p>
    <p>Hello world!</p>
</div>
```

Modules
-------

You can define modules in a file and import them individually:

file.tpl.html

```html
<div class="sidebar">
    ::import sidebar from modules do
        ::set hello
            <p>Hello world!</p>
        ::/set
    ::/import
</div>
```

modules.tpl.html

```html
::module sidebar
    <p>I'm sidebar!</p>
    ::print hello
::/module

::module another
::/module
```

... result:

```html
<div class="sidebar">
    <p>I'm sidebar!</p>
    <p>Hello world!</p>
</div>
```
